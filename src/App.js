import "./App.css";
import "./assets/css/Custom.css";
import "bootstrap/dist/css/bootstrap.min.css";

import HomeComponent from "./components/HomeComponent";
import InvitationComponent from "./components/InvitationComponent";
import NavbarComponent from "./components/NavbarComponent";
import LocationComponent from "./components/LocationComponent";

import { BrowserRouter, Route } from "react-router-dom";
import Switch from "react-bootstrap/esm/Switch";

import React, { useState, useEffect } from "react";
import { useTransition, animated, config } from "react-spring";

const slides1 = [
    {
        id: 0,
        url: require("./assets/images/DSC09127.jpg").default,
    },
    {
        id: 1,
        url: require("./assets/images/DSC09041.jpg").default,
    },
    {
        id: 2,
        url: require("./assets/images/DSC09312.jpg").default,
    },
];

const slides2 = [
    {
        id: 0,
        url: require("./assets/images/DSC09127.jpg").default,
    },
    {
        id: 1,
        url: require("./assets/images/DSC09041.jpg").default,
    },
    {
        id: 2,
        url: require("./assets/images/DSC09091.jpg").default,
    },
];

function App() {
    console.log(window.innerWidth <= 992);
    const [index, set] = useState(0);
    const transitions = useTransition(
        window.innerWidth <= 992 ? slides2[index] : slides1[index],
        (item) => item.id,
        {
            from: { opacity: 0 },
            enter: { opacity: 1 },
            leave: { opacity: 0 },
            config: config.molasses,
        }
    );
    useEffect(
        () => void setInterval(() => set((state) => (state + 1) % 3), 10000),
        []
    );

    return (
        <div className="App">
            <BrowserRouter>
                {transitions.map(({ item, props, key }) => (
                    <animated.div
                        key={key}
                        className="slider"
                        style={{
                            ...props,
                            backgroundImage: `url(${item.url})`,
                        }}
                    />
                ))}
                <NavbarComponent />
                <Switch>
                    <Route exact path="/">
                        <HomeComponent />
                    </Route>
                    <Route path="/invitation">
                        <InvitationComponent />
                    </Route>
                    <Route path="/locations">
                        <LocationComponent />
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
