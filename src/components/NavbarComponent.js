import React from "react";
import "../assets/css/Navbar.css";
import { Navbar, Nav, Container, Image } from "react-bootstrap";
import { NavLink } from "react-router-dom";

const NavbarComponent = (props) => {
    return (
        <div className="nav-border digivit-nav">
            <div className="header-logo">
                <Navbar.Brand href="/">
                    <Image
                        className="img-responsive nav-img"
                        src={require("../assets/images/logo.png").default}
                    />
                </Navbar.Brand>
            </div>
            <Navbar>
                <Container>
                    <Nav
                        defaultActiveKey="/"
                        className="justify-content-center"
                    >
                        <NavLink
                            exact
                            activeClassName="active"
                            className="nav-link"
                            to="/"
                        >
                            HOME
                        </NavLink>
                        <Nav.Link eventKey="disabled" disabled>
                            |
                        </Nav.Link>
                        <NavLink
                            activeClassName="active"
                            className="nav-link"
                            to="/invitation"
                        >
                            INVITATION
                        </NavLink>{" "}
                        <Nav.Link eventKey="disabled" disabled>
                            |
                        </Nav.Link>
                        <NavLink
                            activeClassName="active"
                            className="nav-link"
                            to="/locations"
                        >
                            LOCATIONS
                        </NavLink>{" "}
                        <Nav.Link eventKey="disabled" disabled>
                            |
                        </Nav.Link>
                        <NavLink
                            activeClassName="active"
                            className="nav-link"
                            to="/rsvp"
                        >
                            RSVP
                        </NavLink>{" "}
                    </Nav>
                </Container>
            </Navbar>
        </div>
    );
};

export default NavbarComponent;
