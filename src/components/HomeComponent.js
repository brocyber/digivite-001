import React from "react";
import "../assets/css/Home.css";
import { Image } from "react-bootstrap";

export default class HomeComponent extends React.Component {
    render() {
        return (
            <div className="home">
                <h3 className="center">
                    Love, loughter, and happily ever after
                    <br />
                    <span>02.01.2021</span>
                </h3>
            </div>
        );
    }
}
